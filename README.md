![Licence](https://img.shields.io/badge/license-MIT-blue) ![Build](https://gitlab.com/AmalricBzh/TypoFixerBundle/badges/master/pipeline.svg) ![Coverage](https://gitlab.com/AmalricBzh/TypoFixerBundle/badges/master/coverage.svg)

# Presentation !
TypoFixerBundle is a bundle to quickly fix French typography. That's why
most of doc are in French. If you need an english version, feel free to
talk about it with me, the bundle should handle it in future.

TypoFixerBundle est une façon rapide de corriger certaines erreurs de typographie
française courantes par votre application Symfony. Installez le package avec 
la commande suivante :

```console
composer require amalricbzh/typo-fixer-bundle
```

Et c'est tout ! Si vous n'utilisez *pas* Symfony Flex, vous devrez aussi ajouter
 `AmalricBzh\TypoFixerBundle\AmalricBzhTypoFixerBundle`
dans votre fichier `config/bundles.php`.

## Usage
Ce bundle corrige certaines erreurs (pas listées dans la doc, voir le code
source). Pour l'utiliser :
```php
// src/Controller/SomeController.php
use AmalricBzh\TypoFixerBundle\TypoFixer;
// ...
class SomeController
{
    public function index()
    {
        $ancienTexte = "Texte d' exemple( avec erreurs de typo )!";
        $fixer = new TypoFixer($ancienTexte); 
        $nouveauTexte = $fixer->fix($ancienTexte);
        // "Texte d’exemple (avec erreurs de typo) !"
        dump($nouveauTexte);

        $nouveauTexte = $fixer->text('Autre,texte:de;test...')->fix();
        // "Autre, texte : de ; test…"
        dump($nouveauTexte);

        $nouveauTexte = $fixer->text('Guillemets typographiques et "point final" corrigés')->fix();
        // "Guillemets et « point final » corrigés."
        dump($nouveauTexte);

        $nouveauTexte = $fixer->text('Liste1,liste2,liste3,etc, sans point final')->disableFinalDot()->fix();
        // "Liste1, liste2, liste3, etc., sans point final"
        dump($nouveauTexte);

    }
}
```
Les tests vous donnerons toute une panoplie d'exemples.

Vous pouvez accéder directement au service en utilisant l'id
`amalric_bzh_typo_fixer.typo_fixer`.

## Configuration
Aucune.

## Tests
Lancez la commande suivante :
```
vendor/bin/simple-phpunit
```


## Contributing
*J'ai pompé le texte suivant je ne sais plus où, mais je le pense…* 

Bien sûr, l'open source est alimenté par la capacité de chacun à donner un peu
de son temps pour le plus grand bien. Si vous souhaitez voir une fonctionnalité 
ou en ajouter une autre, génial ! Vous pouvez le demander, mais créer une
merge-request est un moyen encore meilleur de faire avancer les choses.

Dans tous les cas, n'hésitez pas à signaler des problèmes ou à faire des merge-request :
toutes les contributions et les questions sont chaleureusement appréciées :).


