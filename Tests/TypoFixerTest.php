<?php

namespace AmalricBzh\Tests;


use AmalricBzh\TypoFixerBundle\TypoFixer;
use PHPUnit\Framework\TestCase;

class TypoFixerTest extends TestCase
{
    public function testFixEmpty()
    {
        $res = (new TypoFixer(''))->fix();
        $this->assertEquals("", $res);

        $res = (new TypoFixer())->fix('');
        $this->assertEquals('', $res);

        $res = (new TypoFixer('Blabla'))->text('')->fix();
        $this->assertEquals('', $res);

        $res = (new TypoFixer('    '))->fix();
        $this->assertEquals("", $res);

    }

    public function testFixEllipsis()
    {
        // Les points de suspensions sont toujours 3, pas plus.
        // ils ne sont jamais précédés d'une espace justifiante (normale)
        $fixer = new TypoFixer("Texte à trois points ... et encore trois points..... ");

        $res = $fixer->fix();
        $this->assertEquals("Texte à trois points… et encore trois points…", $res);

        // ils sont suivis d'une espace justifiante sauf si le caractère suivant est un crochet ou une parenthèse
        $res = $fixer->text('Texte....avec crochets [ ...] et parenthèses ( ...) ...')->fix();
        $this->assertEquals("Texte… avec crochets […] et parenthèses (…)…", $res);
        // S'il y a une (ou plus) espace entre des points de suspension et le crochet/parenthèse fermants, on la retire
        $res = $fixer->text('Texte....avec crochets [... ] et parenthèses (.... ) ...')->fix();
        $this->assertEquals("Texte… avec crochets […] et parenthèses (…)…", $res);

        // ils disparaissent au profit d'un . s'ils sont précédés de etc
        $res = $fixer->text('Ceci, cela, etc...')->fix();
        $this->assertEquals("Ceci, cela, etc.", $res);

        // ils font disparaître la virgule qui les précède
        $res = $fixer->text('Toto va à la pêche, au ski, au skate, ....')->fix();
        $this->assertEquals("Toto va à la pêche, au ski, au skate…", $res);
    }

    public function testMultiSpaces()
    {
        $fixer = new TypoFixer("Texte avec 5     espaces, des   \t \t tabs, et des espaces      insécables.");

        $res = $fixer->fix();
        $this->assertEquals("Texte avec 5 espaces, des tabs, et des espaces insécables.", $res);

        $res = $fixer->text("Texte    avec   \r\n  des sauts   \r\n de ligne.")->fix();
        $this->assertEquals("Texte avec\r\n des sauts\r\n de ligne.", $res);
    }

    public function testEtc()
    {
        // &c est une mauvaise abréviation de etc. (le 2e &c est suivi d'espaces insécables)
        $fixer = new TypoFixer("Texte avec des &c et des &c et des\t&c. etc, etc. ");

        $res = $fixer->fix();
        $this->assertEquals("Texte avec des etc. et des etc. et des etc. etc., etc.", $res);

        // etc est toujours suivi d'un point
        $res = $fixer->text("Texte avec etc sans point et etc ")->fix();
        $this->assertEquals("Texte avec etc. sans point et etc.", $res);

        // etc. doit normalement toujours être précédé d'une virgule puis d'une espace insécable. Ici on
        // ne corrige que l'absence d'espace insécable
        $res = $fixer->text("Des etc. sans espace insécable avant\tetc. ou  etc.")->fix();
        $this->assertEquals("Des etc. sans espace insécable avant etc. ou etc.", $res);
    }

    public function testDots()
    {
        // Deux points consécutifs => faute de frappe, un seul point
        $fixer = new TypoFixer("Fin de phrase.. Nouvelle phrase.");
        $res = $fixer->fix();
        $this->assertEquals("Fin de phrase. Nouvelle phrase.", $res);

        // Jamais d'espace avant les points
        $res = $fixer->text("Un point .Nouvelle phrase\t. Et encore .Encore.")->fix();
        $this->assertEquals("Un point. Nouvelle phrase. Et encore. Encore.", $res);

        // Un point est toujours suivi d'un blanc normal (sauf si c'est déjà le cas).
        $res = $fixer->text("Un point.Nouvelle phrase.\tEt encore. Encore.")->fix();
        $this->assertEquals("Un point. Nouvelle phrase.\tEt encore. Encore.", $res);
    }

    public function testCommas()
    {
        // Jamais d'espace avant les virgules
        $fixer = new TypoFixer("Bonjour , Ceci est un texte avec des , virgules\n, fin");
        $res = $fixer->fix();
        $this->assertEquals("Bonjour, Ceci est un texte avec des, virgules, fin.", $res);

        // Un point est toujours suivi d'un blanc normal (sauf si c'est déjà le cas).
        $res = $fixer->text("Bonjour ,Ceci est un texte avec des, virgules,\nfin")->fix();
        $this->assertEquals("Bonjour, Ceci est un texte avec des, virgules,\nfin.", $res);
    }

    public function testColumns()
    {
        // Que des espaces insécables avant
        $fixer = new TypoFixer("Deux points: et points:deux : points : là");
        $res = $fixer->fix();
        $this->assertEquals("Deux points : et points : deux : points : là.", $res);
    }

    public function testSemiColumns()
    {
        // Que des espaces insécables avant
        $fixer = new TypoFixer("Points;virgules et points;virgule ; points ; là");
        $res = $fixer->fix();
        $this->assertEquals("Points ; virgules et points ; virgule ; points ; là.", $res);
    }

    public function testQuestionMark()
    {
        // Que des espaces insécables avant
        $fixer = new TypoFixer("Points?interrogation et points?interrogation ? points ? ici");
        $res = $fixer->fix();
        $this->assertEquals("Points ? interrogation et points ? interrogation ? points ? ici.", $res);
    }

    public function testExclamationMark()
    {
        // Que des espaces insécables avant
        $fixer = new TypoFixer("Points!exclamation et points!exclamation ! points ! ici");
        $res = $fixer->fix();
        $this->assertEquals("Points ! exclamation et points ! exclamation ! points ! ici.", $res);
    }

    public function testParenthesis()
    {
        $fixer = new TypoFixer("Texte(avec)parenthèses ( un ) peu ( partout ) texte");
        $res = $fixer->fix();
        $this->assertEquals("Texte (avec) parenthèses (un) peu (partout) texte.", $res);
    }

    public function testBrackets()
    {
        $fixer = new TypoFixer("Texte[avec]crochets [ un ] peu [ partout ] texte");
        $res = $fixer->fix();
        $this->assertEquals("Texte [avec] crochets [un] peu [partout] texte.", $res);
    }

    public function testSingleQuote()
    {
        $fixer = new TypoFixer("C'est l'apostrophe j ' aime l ' apostrophe");
        $res = $fixer->fix();
        $this->assertEquals("C’est l’apostrophe j’aime l’apostrophe.", $res);
    }

    public function testDoubleQuote()
    {
        $fixer = new TypoFixer('Il " met " des doubles"quotes"pas " standards " ');
        $res = $fixer->fix();
        $this->assertEqualsMod('Il « met » des doubles « quotes » pas « standards ».', $res);
    }

    public function testCombinations()
    {
        $fixer = new TypoFixer("Texte[???]avec ( !!! ) \"combinaisons\" . (...)... ! ? ? !");
        $res = $fixer->fix();
        $this->assertEquals("Texte [???] avec (!!!) « combinaisons ». (…)… !??!", $res);
    }

    public function testFinalDot()
    {
        $fixer = new TypoFixer("Texte sans point final");
        $res = $fixer->fix();
        $this->assertEquals("Texte sans point final.", $res);

        $res = $fixer->text('Texte !')->fix();
        $this->assertEquals("Texte !", $res);

        $res = $fixer->text('Texte ?')->fix();
        $this->assertEquals("Texte ?", $res);

        $res = $fixer->text('Texte …')->fix();
        $this->assertEquals("Texte…", $res);
    }


    public function testConfig()
    {
        $fixer = new TypoFixer("Texte sans point final");
        $res = $fixer->disableFinalDot()->fix();
        $this->assertEquals("Texte sans point final", $res);
        $res = $fixer->enableFinalDot()->fix();
        $this->assertEquals("Texte sans point final.", $res);
    }

    public function testGeneaDegree()
    {
        $text = "Jean Duval\r\n" .
            "I.a Pierre Duval (aîné), capitaine d'une compagnie, épouse Louise Durand.\r\n" .
            "II Jean II Duval, né vers 1590, épouse Jeanne Robert \r\n" .
            "III Pierre II Duval épouse Anne Le Jeune.\r\n" .
            "IV.a Jean III Duval, conseiller au présidial d'Angers, épouse Martine du Pont.\r\n" .
            "IV.b Jacques Duval (cadet), né vers 1657, épouse Françoise Martin ,puis Françoise Durand\r\n" .
            "    IV.c Antoine Duval(sans alliance ) .\r\n" .
            "IV.d-f Trois filles religieuses\r\n" .
            " I.b Jacques Duval épouse Martine Dupond    \r\n" .
            " II Jean-François Duval, contrôleur général des armées . \r\n" .
            " III.a Pierre Duval( sans alliance) .\r\n" .
            "III.b Jeanne Duval épouse Antoine du Puis, conseiller du roi   \r\n" .
            "     III.c Jacques Duval, recteur de Saint-Martin\r\n" .
            "III.d-e Deux filles non mariées  \r\n";
        $expected = "Jean Duval.\n" .
            "I.a Pierre Duval (aîné), capitaine d’une compagnie, épouse Louise Durand.\n" .
            "II Jean II Duval, né vers 1590, épouse Jeanne Robert.\n" .
            "III Pierre II Duval épouse Anne Le Jeune.\n" .
            "IV.a Jean III Duval, conseiller au présidial d’Angers, épouse Martine du Pont.\n" .
            "IV.b Jacques Duval (cadet), né vers 1657, épouse Françoise Martin, puis Françoise Durand.\n" .
            "IV.c Antoine Duval (sans alliance).\n" .
            "IV.d-f Trois filles religieuses.\n" .
            "I.b Jacques Duval épouse Martine Dupond.\n" .
            "II Jean-François Duval, contrôleur général des armées.\n" .
            "III.a Pierre Duval (sans alliance).\n" .
            "III.b Jeanne Duval épouse Antoine du Puis, conseiller du roi.\n" .
            "III.c Jacques Duval, recteur de Saint-Martin.\n" .
            "III.d-e Deux filles non mariées.";
        $fixer = new TypoFixer($text);
        $res = $fixer->enableGeneaDegrees()->fix();
        $this->assertEquals($expected, $res);
    }

    public function testComplete()
    {
        $text = "Ah!non!c'est un peu court ,jeune homme!
On pouvait dire....Oh! Dieu!… bien des choses en somme...
En variant le ton, – par exemple ,tenez :
Agressif:\"Moi, monsieur, si j’avais un tel nez,
Il faudrait sur-le-champ que je me l'amputasse!\"
Amical : \"Mais il doit tremper dans votre tasse
Pour boire ,faites-vous fabriquer un hanap!\"
Descriptif:« C’est un roc !…c'est un pic!… c ' est un cap!
Que dis-je(du verbe dire) , c’est un cap?…C’est une péninsule! »
Curieux:«De quoi sert cette oblongue capsule ?
D’écritoire, monsieur [ M.], ou de boîte à ciseaux ?» ";

        $expected = "Ah ! non ! c’est un peu court, jeune homme !
On pouvait dire… Oh ! Dieu ! … bien des choses en somme…
En variant le ton, – par exemple, tenez :
Agressif : « Moi, monsieur, si j’avais un tel nez,
Il faudrait sur-le-champ que je me l’amputasse ! »
Amical : « Mais il doit tremper dans votre tasse
Pour boire, faites-vous fabriquer un hanap ! »
Descriptif : « C’est un roc ! … c’est un pic ! … c’est un cap !
Que dis-je (du verbe dire), c’est un cap ? … C’est une péninsule ! »
Curieux : « De quoi sert cette oblongue capsule ?
D’écritoire, monsieur [M.], ou de boîte à ciseaux ? ».";

        $fixer = new TypoFixer($text);
        $res = $fixer->fix();
        $this->assertEqualsMod($expected, $res);
    }




    private function assertEqualsMod($expected, $actual, $message='')
    {
        $expected = str_replace(' ', '£', $expected);
        $actual = str_replace(' ', '£', $actual);
        $this->assertEquals($expected, $actual, $message);
    }


}