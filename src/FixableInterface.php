<?php

namespace AmalricBzh\TypoFixerBundle;

interface FixableInterface
{
    public function fixTypo(): void;
}
