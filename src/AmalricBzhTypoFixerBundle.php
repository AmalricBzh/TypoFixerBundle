<?php

namespace AmalricBzh\TypoFixerBundle;

use AmalricBzh\TypoFixerBundle\DependencyInjection\AmalricBzhTypoFixerExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AmalricBzhTypoFixerBundle extends Bundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        if (null === $this->extension) {
            $this->extension = new AmalricBzhTypoFixerExtension();
        }

        return $this->extension;
    }
}
