<?php

namespace AmalricBzh\TypoFixerBundle\EventListener;

use AmalricBzh\TypoFixerBundle\FixableInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events as DoctrineEvents;

class FixableEventListener implements EventSubscriber
{
    public function getSubscribedEvents(): array
    {
        return [
            DoctrineEvents::prePersist,
            DoctrineEvents::preUpdate,
        ];
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getEntity();
        if ($entity instanceof FixableInterface) {
            $entity->fixTypo();
        }
    }

    public function preUpdate(LifecycleEventArgs $args): void
    {
        $entity = $args->getEntity();
        if ($entity instanceof FixableInterface) {
            $entity->fixTypo();
        }
    }
}
