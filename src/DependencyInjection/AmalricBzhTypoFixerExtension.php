<?php

namespace AmalricBzh\TypoFixerBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class AmalricBzhTypoFixerExtension extends Extension
{
    /**
     * @param array<string, mixed> $configs
     * @throws \Exception
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');
        $loader->load('listeners.xml');
    }

    public function getAlias(): string
    {
        return 'amalric_bzh_typo_fixer';
    }
}
