<?php

namespace AmalricBzh\TypoFixerBundle;

class TypoFixer
{
    /**
     * Voir http://jacques-andre.fr/faqtypo/lessons.pdf
     */

    /**
     * @var string
     */
    private $text;
    /**
     * @var string
     */
    private $originalText;
    /**
     * @var bool
     */
    private $finalDot = true;

    /**
     * @var bool
     */
    private $geneaDegrees = false;

    public function __construct(string $text='')
    {
        /* Utilise l'encodage interne UTF-8 */
        mb_internal_encoding("UTF-8");
        $this->originalText = $text;
    }

    public function text(string $text): self
    {
        $this->originalText = $text;

        return $this;
    }

    public function disableFinalDot(): self
    {
        $this->finalDot = false;

        return $this;
    }

    public function enableFinalDot(): self
    {
        $this->finalDot = true;

        return $this;
    }

    public function enableGeneaDegrees(): self
    {
        $this->geneaDegrees = true;

        return $this;
    }

    public function disableGeneaDegrees(): self
    {
        $this->geneaDegrees = false ;

        return $this;
    }

    public function fix(): string
    {
        $this->text = $this->originalText;
        // *** L'ordre des règles est important ! ***
        $this->text = trim($this->text);
        // Si on n'a plus rien à ce niveau-là, c'est qu'on a une chaîne vide.
        if (strlen($this->text) === 0) {
            return $this->text;
        }
        $this->ruleMultispaces();
        $this->ruleEllipsis();
        $this->ruleEtc();
        $this->ruleDots();
        $this->ruleCommas();
        $this->ruleColumns();
        $this->ruleSemiColumns();
        $this->ruleQuestionMark();
        $this->ruleExclamationMark();
        $this->ruleParenthesis();
        $this->ruleBrackets();
        $this->ruleSingleQuote();
        $this->ruleDoubleQuote();

        $this->rulesCombinations();

        if ($this->geneaDegrees) {
            $this->ruleGeneaDegrees();
        }

        $this->text = trim($this->text);

        if ($this->finalDot) {
            $this->ruleFinalDot();
        }

        return $this->text;
    }


    private function ruleEllipsis(): void
    {
        // ALT + 0133
        // Les points de suspensions sont toujours 3, pas plus.
        $text = preg_replace('/\.{3,}/', '…', $this->text);
        assert(is_string($text));
        // ils ne sont jamais précédés d'une espace justifiante (normale)
        $text = $this->removeSpaceBefore('…', $text);

        // ils sont suivis d'une espace justifiante
        $text = $this->addBSpaceAfter('…', $text);

        // ils disparaissent au profit d'un . s'ils sont précédés de etc
        $text = str_replace(' etc…', ' etc.', $text);

        // ils font disparaître la virgule qui les précède
        $text = str_replace(',…', '…', $text);
        assert(is_string($text));
        $this->text = $text;
    }

    private function ruleMultispaces(): void
    {
        // ALT + 0160 ; Pas de \s qui inclut les \r et \n !
        // espace normales, insécables ou tabulation multiples par une seule espace normale
        $text = preg_replace("/[ \x{00A0}\t]{2,}/u", ' ', $this->text);
        assert(is_string($text));

        // espaces insécables multiples (ALT+0160) par une seule.
        $text = preg_replace("/\x{00A0}/u", " ", $text);
        assert(is_string($text));

        // espaces en fin de ligne supprimées
        $text = preg_replace("/[ \x{00A0}\t](?=[\r\n])/u", "", $text);
        assert(is_string($text));
        $this->text = $text;
    }

    private function ruleEtc(): void
    {
        // &c est une mauvaise abréviation de etc. (forma ancienne, non retenue pour transcriptions)
        $text = preg_replace('/([\s\x{00A0}])&c\.?([\s\x{00A0},])/u', "$1etc.$2", $this->text);
        assert(is_string($text));
        // etc. est toujours suivi d'un point.
        $text = preg_replace('/([\s\x{00A0}])etc([\s\x{00A0},])/u', "$1etc.$2", $text);
        assert(is_string($text));

        // etc. doit normalement toujours être précédé d'une virgule puis d'une espace insécable. Ici on
        // ne corrige que l'absence d'espace insécable
        $this->text = $this->addUBSpaceBefore('etc.', $text);
    }

    private function ruleDots(): void
    {
        // A utiliser après le traitement des points de suspensions.
        // Deux points consécutifs => faute de frappe, un seul point
        $text = str_replace('..', '.', $this->text);
        assert(is_string($text));

        // Jamais d'espace avant les points
        $text = $this->removeSpaceBefore('.', $text);

        // Un point est toujours suivi d'un blanc normal
        $this->text = $this->addBSpaceAfter('.', $text);
    }

    private function ruleCommas(): void
    {
        // Jamais d'espace avant les virgules
        $this->text = $this->removeSpaceBefore(',', $this->text);

        // toujours une espace normale après les virgules (ou saut de ligne ou tab)
        $this->text = $this->addBSpaceAfter(',', $this->text);
    }

    private function ruleColumns(): void
    {
        // Toujours une espace insécable avant. On remplace si c'est précédé d'une autre espace
        $this->text = $this->addUBSpaceBefore(':', $this->text);
        // Et ils sont toujours suivis d'une espace normale.
        $this->text = $this->addBSpaceAfter(':', $this->text);
    }

    private function ruleSemiColumns(): void
    {
        // Toujours une espace insécable avant. On remplace si c'est précédé d'une autre espace
        $this->text = $this->addUBSpaceBefore(';', $this->text);
        // Et ils sont toujours suivis d'une espace normale.
        $this->text = $this->addBSpaceAfter(';', $this->text);
    }

    private function ruleQuestionMark(): void
    {
        // Toujours une espace insécable avant. On remplace si c'est précédé d'une autre espace
        $this->text = $this->addUBSpaceBefore('?', $this->text);
        // Et toujours suivi d'une espace normale.
        $this->text = $this->addBSpaceAfter('?', $this->text);
    }

    private function ruleExclamationMark(): void
    {
        // Toujours une espace insécable avant. On remplace si c'est précédé d'une autre espace
        $this->text = $this->addUBSpaceBefore('!', $this->text);
        // Et toujours suivi d'une espace normale.
        $this->text = $this->addBSpaceAfter('!', $this->text);
    }

    private function ruleParenthesis(): void
    {
        // espace sécable avant l'ouvrante
        $this->text = $this->addBSpaceBefore('(', $this->text);
        // pas d'espace après l'ouvrante
        $this->text = $this->removeSpaceAfter('(', $this->text);
        // pas d'espace avant la fermante
        $this->text = $this->removeSpaceBefore(')', $this->text);
        // espace après la fermante
        $this->text = $this->addBSpaceAfter(')', $this->text);
    }

    private function ruleBrackets(): void
    {
        // espace sécable avant l'ouvrante
        $this->text = $this->addBSpaceBefore('[', $this->text);
        // pas d'espace après l'ouvrante
        $this->text = $this->removeSpaceAfter('[', $this->text);
        // pas d'espace avant la fermante
        $this->text = $this->removeSpaceBefore(']', $this->text);
        // espace après la fermante
        $this->text = $this->addBSpaceAfter(']', $this->text);
    }

    /**
     * Remplacement des apostrophes
     */
    private function ruleSingleQuote(): void
    {
        $text = str_replace("'", '’', $this->text);
        assert(is_string($text));
        $this->text = $this->removeSpaceBefore('’', $text);
        $this->text = $this->removeSpaceAfter('’', $this->text);
    }


    private function ruleDoubleQuote(): void
    {
        // "toto" => « toto » ; Ne pas utiliser si le texte contient des balises html avec attributs !
        $text = preg_replace('/"([^"]*)"/u', '«$1»', $this->text);
        assert(is_string($text));

        $this->text = $this->addBSpaceBefore('«', $text);
        $this->text = $this->addBSpaceAfter('»', $this->text);
        $this->text = $this->addUBSpaceAfter('«', $this->text);
        $this->text = $this->addUBSpaceBefore('»', $this->text);
    }

    private function rulesCombinations(): void
    {
        $this->text = $this->removeSpaceBetween('?', '?', $this->text);
        $this->text = $this->removeSpaceBetween('!', '!', $this->text);
        $this->text = $this->removeSpaceBetween('?', '!', $this->text);
        $this->text = $this->removeSpaceBetween('!', '?', $this->text);
        $this->text = $this->removeSpaceBetween(')', '…', $this->text);
        $this->text = $this->removeSpaceBetween(')', '.', $this->text);
        $this->text = $this->removeSpaceBetween(')', ',', $this->text);
        $this->text = $this->removeSpaceBetween(']', '…', $this->text);
        $this->text = $this->removeSpaceBetween(']', '.', $this->text);
        $this->text = $this->removeSpaceBetween(']', ',', $this->text);
        $this->text = $this->removeSpaceBetween('»', ',', $this->text);
        $this->text = $this->removeSpaceBetween('»', '.', $this->text);
        $this->text = $this->removeSpaceBetween('»', '…', $this->text);
    }

    private function ruleFinalDot(): void
    {
        // Point final si on n'a pas déjà des points de suspension à la fin, un ? ou !
        $lastCharMb = mb_substr($this->text, -1);
        $lastChar = substr($this->text, -1);
        if ($lastCharMb !== '…' && $lastChar !== '.' && $lastChar !== '?' && $lastChar !== '!') {
            $this->text .= '.';
        }
    }

    private function ruleGeneaDegrees(): void
    {
        // Suppression des espaces en trop en début de ligne
        $text = preg_replace("/^[\s]*/m", "", $this->text);
        assert(is_string($text));
        // Les I, II, III.a, IX.b-c, VII.c-f en début de ligne doivent être conservés
        $text = preg_replace("/^([IXV]+)\.[ ]?([a-z])(-[a-z])? /m", "$1.$2$3 ", $text);
        assert(is_string($text));
        // Suppression des espaces en trop en fin de ligne (supprime aussi les \r)
        $text = preg_replace("/[\s]*$/m", "", $text);
        assert(is_string($text));
        // Ajout d'un point final si nécessaire en fin de ligne
        $text = preg_replace("/([^\.!?…])$/m", "$1.", $text);
        assert(is_string($text));
        $this->text = $text;
    }

    private function sanitizeForRegexp(string $text): string
    {
        $text = str_replace('.', '\.', $text);
        $text = str_replace('?', '\?', $text);
        $text = str_replace('(', '\(', $text);
        $text = str_replace(')', '\)', $text);
        $text = str_replace('[', '\[', $text);
        $text = str_replace(']', '\]', $text);

        return $text;
    }


    private function removeSpaceBefore(string $pattern, string $text): string
    {
        $regexPattern =$this->sanitizeForRegexp($pattern);
        $text = preg_replace("/[\s]$regexPattern/u", $pattern, $text);
        assert(is_string($text));

        return $text;
    }

    private function removeSpaceAfter(string $pattern, string $text): string
    {
        $regexPattern =$this->sanitizeForRegexp($pattern);
        $text = preg_replace("/{$regexPattern}[\s ]/u", $pattern, $text);
        assert(is_string($text));

        return $text;
    }

    /**
     * Add breakable space after. Breakable are all spaces but unbreakable space, ie tab, return...
     * @param string $pattern
     * @param string $text
     */
    private function addBSpaceAfter(string $pattern, string $text): string
    {
        $regexPattern = $this->sanitizeForRegexp($pattern);
        // unbreakable spaces after pattern are removed
        $text = preg_replace("/{$regexPattern} /u", "$pattern", $text);
        assert(is_string($text));
        // breakeable space after added if not already one
        $text = preg_replace("/{$regexPattern}(?!\s)/u", "$pattern ", $text);
        assert(is_string($text));
        return $text;
    }

    private function addBSpaceBefore(string $pattern, string $text): string
    {
        $regexPattern = $this->sanitizeForRegexp($pattern);
        // unbreakable spaces before pattern are removed
        $text = preg_replace("/ {$regexPattern}/u", "$pattern", $text);
        assert(is_string($text));
        // breakeable space before added if not already one
        $text = preg_replace("/(?<!\s){$regexPattern}/u", " $pattern", $text);
        assert(is_string($text));

        return $text;
    }


    /** Add unbreakable space before */
    private function addUBSpaceBefore(string $pattern, string $text): string
    {
        $regexPattern = $this->sanitizeForRegexp($pattern);

        // breakable spaces before pattern are removed
        $text = preg_replace("/[\s]({$regexPattern})/u", "$1", $text);
        assert(is_string($text));
        // If previous char is not unbreakable space, we add one
        $text = preg_replace("/(?<! )({$regexPattern})/u", " $1", $text);
        assert(is_string($text), 'Replaced text is not a string.');

        return $text;
    }

    private function addUBSpaceAfter(string $pattern, string $text): string
    {
        $regexPattern = $this->sanitizeForRegexp($pattern);

        // breakable spaces after pattern are removed
        $text = preg_replace("/({$regexPattern})[\s]/u", "$1", $text);
        assert(is_string($text));
        // If following char is not unbreakable space, we add one
        $text = preg_replace("/({$regexPattern})(?! )/u", "$1 ", $text);
        assert(is_string($text), 'Replaced text is not a string.');

        return $text;
    }

    private function removeSpaceBetween(string $first, string $second, string $text): string
    {
        $regexFirst = $this->sanitizeForRegexp($first);
        $regexSecond = $this->sanitizeForRegexp($second);

        $text = preg_replace("/{$regexFirst}\s{$regexSecond}/u", "{$first}{$second}", $text);
        assert(is_string($text));
        // Si first === second, on a fait ? ? ? => ?? ? ; il faut refaire une passe
        if ($first === $second) {
            $text = preg_replace("/{$regexFirst}\s{$regexSecond}/u", "{$first}{$second}", $text);
            assert(is_string($text));
        }

        return $text;
    }
}
